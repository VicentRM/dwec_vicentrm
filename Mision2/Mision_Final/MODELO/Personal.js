
function Personal(nom,ape,esp){
    this.nombre=nom;
    this.apellidos=ape;
    this.especialidad=esp; //tipo objeto especialidad

    //Metodos get y set

    this.getNombre=function() {
        return this.nombre;
    }
    this.setNombre=function(nom) {
        this.nombre=nom;
    }
    this.getApellidos=function() {
        return this.apellidos;
    }
    this.setApellidos=function(ape) {
        this.apellidos=ape;
    }
    this.getEspecialidad=function() {
        switch (this.especialidad){
            case 1:
                return "Medico";
                break;
            case 2:
                return "Enfermero/a";
                break;
            case 3:
                return "Celador/a"
                break;
        }
    }
    this.setEspecialidad=function(esp) {
        this.especialidad=esp;
    }
    //Metodos
    this.mostrar=function() {
        return this.getNombre() + " " + this.getApellidos() + " " + this.getEspecialidad();
    }

}
