function Paciente(nom,ape,pers){
    this.nombre=nom;
    this.apellidos=ape;
    this.personal=pers;

    this.getNombre=function() {
        return this.nombre;
    }
    this.setNombre=function(nom) {
        this.nombre=nom;
    }
    this.getApellidos=function() {
        return this.apellidos;
    }
    this.setApellidos=function(ape) {
        this.apellidos=ape;
    }
    this.getPersonal=function(){
        return this.personal();
    }
    this.setPersonal=function(pers){
        this.personal=pers;
    }
}