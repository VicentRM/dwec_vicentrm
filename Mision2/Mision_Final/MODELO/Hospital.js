function Hospital(){

}
function Hospital(nom,loc,resp,cap){
    this.nombre=nom;
    this.localidad=loc;
    this.responsable=resp; //el responsable sera un objeto de tipo medico
    this.capacidad=cap;

    //Metodos get y set
    this.getNombre=function() {
        return this.nombre;
    }
    this.setNombre=function(nom) {
        this.nombre=nom;
    }
    this.getLocalidad=function() {
        return this.localidad;
    }
    this.setLocalidad=function(loc) {
        this.localidad=loc;
    }
    this.getResponsable=function() {
        return this.responsable.getNombre();
    }
    this.setResponsable=function(resp) {
        this.responsable=resp;
    }
    this.getCapacidad=function() {
        return this.capacidad;
    }
    this.setCapacidad=function(cap) {
        this.capacidad=cap;
    }
    //Metodos
    this.mostrar=function() {
        return this.getNombre() + " " + this.getLocalidad() + " " + this.getResponsable() + " " + this.getCapacidad();
    }
}
/*
<script>
localStorage.setItem("apellido", "Garcia");
alert(localStorage.getItem("apellido"));
localStorage.removeItem("apellido");
alert(localStorage.getItem("apellido"));
</script>
*/