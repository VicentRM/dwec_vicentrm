function insertarPrincipio(lista,sandkill){
    lista.unshift(sandkill); //Insertamos en el array recibido un sandkill al principio
    return lista; //devolvemos el array resultante
}
function insertarFinal(lista,sandkill){
    lista.push(sandkill); //Insertamos en el array recibido un sandkill al principio
    return lista; //devolvemos el array resultante
}
function borrarPrimero(lista){
    lista.shift();
    return lista;
}

function borrarUltimo(lista){
    lista.pop();
    return lista;
}

function mostrar(lista){
    for(var i=0;i<lista.length;i++){    //recorremos array y mostramos
        document.write(lista[i].getNombre()+"<br/>");
    }
}

function ordenar(lista){
    lista.sort(ordena_objetos);
}

function ordena_objetos(a,b){
    if (a.getNombre() < b.getNombre()) return -1;
    else if (a.getNombre() > b.getNombre()) return 1;
    else return 0;
}

function buscarSandkillNombre(lista,nombre){
    for(var i=0;i<lista.length;i++){
        if(lista[i].getNombre()==nombre){  //recorremos el array y si encontramos el nombre lo guardamos en sandkill
            sandkill=lista[i];
        }
    }
    return sandkill;
}

function buscarSandkillPos(lista,posicion){
    sandkill=lista[posicion];
    return sandkill;
}
