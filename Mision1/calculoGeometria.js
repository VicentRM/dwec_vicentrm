function areaTriangulo(b,a){
    return (b*a)/2;
}
function permietroTrianguloEquilatero(l){
    return (Math.sqrt(3)/4)*Math.pow(1,2);
}
function perimetroTrianguloIsosceles(l1, l2){
    return (2*l1)+l2;
}

function permietroTrianguloEscaleno(l1,l2,l3){
    return l1+l2+l3;
}
function areaCuadrado(l){
    return l*l;
}
function perimetroCuadrado(l){
    return l*4;
}
function areaRectangulo(l1,l2){
    return l1*l2;
}
function areaCirculo(r){
    return Math.PI*Math.pow(r,2);
}
function longitudCircunferencia(r){
    return 2*Math.PI*r;
}