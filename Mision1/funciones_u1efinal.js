

//Declaramos el array de objetos de categorias, preguntas y respuestas

var pCategorias=[{id:1,categoria:"Geografia", pregunta:"¿Dónde se ubica el gran desierto de Gobi?", respuesta:"Asia"},
                {id:2,categoria:"Arte", pregunta:"¿En que museo está la Monalisa?", respuesta:"Lourve"},
                {id:3,categoria:"Espectaculos", pregunta:"¿De dónde proviene el reggae?", respuesta:"Jamaica"},
                {id:4,categoria:"Historia", pregunta:"¿En qué país existió una ciudad llamada Heliópolis?", respuesta:"Egipto"},
                {id:5,categoria:"Ciencas", pregunta:"¿Qué enfermedad se caracteriza por producir graves defectos en la coagulación de la sangre?", respuesta:"Hemofilia"},
                {id:6,categoria:"Deportes", pregunta:"¿A qué distancia está situado el punto de penalti de la portería?", respuesta:"11 metros"}
                ];

var animal = {categoria:"Geografia", pregunta:"¿Dónde se ubica el gran desierto de Gobi?", respuesta:"Asia"};
var puntos=0;
var pAcertada=0;
var pError=0;

function mostrarCategorias(){

    //mostramos categorias mientras los aciertos no sean iguales que 4 o los errores iguales q 3
    while(pAcertada<4 && pError<3){
        document.write("<b><u>Elige una una categoria:</u></b><br>");
        var i=0;
        for(i=0;i<pCategorias.length;i++){
            document.write("<ul>");
            document.write("<li>"+pCategorias[i].id+"."+pCategorias[i].categoria+"</li>");
            document.write("</ul>");
        }
        //llamada a la funcion preguntar
        preguntar();
   }
    if(pAcertada==4){
        document.write("<h1>FELICIDADES HAS GANADO LA PARTIDA!</h1>");
    }else if(pError==3){
        document.write("<h1>HAS PERDIDO!</h1>");
    }

}
function preguntar(){
    var pElegida=0;
    var respuesta="";
    var i=0;
    //recogemos el numero de categoria elegida por el jugador
    pElegida=prompt("Eliga categoria(introduzca numero cateogria):");
    for(i=0;i<pCategorias.length;i++){   //con este for recorremos el array de objetos pcateogrias
        if(pCategorias[i].id==pElegida){ //si encontramos el objeto correspondiente a la pregunta elegida le preguntamos al jugador la pregunta
            respuesta=prompt(pCategorias[i].pregunta); //recogemos la respuesta
            if(respuesta==pCategorias[i].respuesta){  //si la respuesta introducida por el jugador es igual a la respuesta guardada en nuestro array le sumamos puntos, sino error!
                pAcertada=pAcertada+1;
                puntos=puntos+1;
                document.write("CORRECTO!:Preguntas acertadas:"+pAcertada+". Preguntas erroneas:"+pError+"<br>");
            }else{
                pError=pError+1;
                document.write("ERROR!:Preguntas acertadas:"+pAcertada+". Preguntas erroneas:"+pError+"<br>");
            }
            pCategorias.splice(i,1);  //borramos el elemento del array para que no lo vuelva a mostrar
        }
    }
}