//Funciones del triangulo declaradas como anonimas
src="variablesGeometria.js"
var areaTriangulo=function (b,a){
    return (b*a)/2;
}
var permietroTrianguloEquilatero=function (l){
    return (Math.sqrt(3)/4)*Math.pow(1,2);
}
var perimetroTrianguloIsosceles=function (l1, l2){
    return (2*l1)+l2;
}

var permietroTrianguloEscaleno=function (l1,l2,l3){
    return l1+l2+l3;
}

//Funciones de paralelogramos definidas e invocadas con el contructor function  var miFuncion=new Function ("a","b","return a*b;");
var areaCuadrado=new Function("l","return l*l");
var perimetroCuadrado=new Function ("l","return 1*4");
var areaRectangulo=new Function ("l1","l2","return l1*l2");

 //Funciones de círculo y cirncunferencia como funciones anónimas autoinvocadas (function () {alert("!Hola");}());

(function (r) {alert("el area del circulo es:"+Math.PI*Math.pow(radio,2));}());
(function (r) {alert("la longitud de la circunferencia es:"+2*Math.PI*radio);}());
