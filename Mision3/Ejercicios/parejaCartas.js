
function Carta(id,nombre){
    this.id=id;
    this.nombre=nombre;
    //Metodos get y set
    this.getId=function() {
        return this.getId;
    }
    this.setId=function(id) {
        this.getId=id;
    }
    this.getNombre=function() {
        return this.nombre;
    }
    this.setNombre=function(nom) {
        this.nombre=nom;
    }

    //Metodos
    this.mostrar=function() {
        return this.getId() + " " + this.getNombre();
    }
}


var rutaCartas="img_cartas/cartasJuego/";
//creamos 1 objeto carta por cada carta que tenemos en pantalla
var carta1=new Carta(1,"1bastos.jpg");
var carta2=new Carta(2,"1espadas.jpg");
var carta3=new Carta(3,"6copas.jpg");
var carta4=new Carta(4,"7espadas.jpg");
var carta5=new Carta(5,"7oros.jpg");
var carta6=new Carta(6,"11bastos.jpg");
var carta7=carta1;
var carta8=carta3;
var carta9=carta5;
var carta10=carta4;
var carta11= carta2;
var carta12=carta6;

//asignacion de los eventos click a cada una de las imagenes del documento html
document.getElementById("carta1").addEventListener("click", voltearImagen);
document.getElementById("carta2").addEventListener("click", voltearImagen);
document.getElementById("carta3").addEventListener("click", voltearImagen);
document.getElementById("carta4").addEventListener("click", voltearImagen);
document.getElementById("carta5").addEventListener("click", voltearImagen);
document.getElementById("carta6").addEventListener("click", voltearImagen);
document.getElementById("carta7").addEventListener("click", voltearImagen);
document.getElementById("carta8").addEventListener("click", voltearImagen);
document.getElementById("carta9").addEventListener("click", voltearImagen);
document.getElementById("carta10").addEventListener("click", voltearImagen);
document.getElementById("carta11").addEventListener("click", voltearImagen);
document.getElementById("carta12").addEventListener("click", voltearImagen);

//Arrays para almacenar los objetos girados y las id de los objetos girados
var arrayGiradas=new Array();
var arrayImgGiradas=new Array();

//funcion manejadora del evento click
function voltearImagen(e){
    //Valoramos la posibilidad de que se utilice un navegador de Microsoft
    if (!e) e = window.event;
    var giradas=0;
    switch (e.target.id) {
        case "carta1":
            //giramos la carta asignandole la imagen correspondiente al objeto
            document.getElementById(e.target.id).src=rutaCartas+carta1.getNombre();
            //llamamos a la funcion comprobar pareja
            comprobarPareja(carta1,e.target.id);
            break;
        case "carta2":
            document.getElementById(e.target.id).src=rutaCartas+carta2.getNombre();
            comprobarPareja(carta2,e.target.id);
            break;
        case "carta3":
            document.getElementById(e.target.id).src=rutaCartas+carta3.getNombre();
            comprobarPareja(carta3,e.target.id);
            break;
        case "carta4":
            document.getElementById(e.target.id).src=rutaCartas+carta4.getNombre();
            comprobarPareja(carta4,e.target.id);
            break;
        case "carta5":
            document.getElementById(e.target.id).src=rutaCartas+carta5.getNombre();
            comprobarPareja(carta5,e.target.id);
            break;
        case "carta6":
            document.getElementById(e.target.id).src=rutaCartas+carta6.getNombre();
            comprobarPareja(carta6,e.target.id);
            break;
        case "carta7":
            document.getElementById(e.target.id).src=rutaCartas+carta7.getNombre();
            comprobarPareja(carta7,e.target.id);
            break;
        case "carta8":
            document.getElementById(e.target.id).src=rutaCartas+carta8.getNombre();
            comprobarPareja(carta8,e.target.id);
            break;
        case "carta9":
            document.getElementById(e.target.id).src=rutaCartas+carta9.getNombre();
            comprobarPareja(carta9,e.target.id);
            break;
        case "carta10":
            document.getElementById(e.target.id).src=rutaCartas+carta10.getNombre();
            comprobarPareja(carta10,e.target.id);
            break;
        case "carta11":
            document.getElementById(e.target.id).src=rutaCartas+carta11.getNombre();
            comprobarPareja(carta11,e.target.id);
            break;
        case "carta12":
            document.getElementById(e.target.id).src=rutaCartas+carta12.getNombre();
            comprobarPareja(carta11,e.target.id);
            break;
    }


}
function comprobarPareja(cartaGirada,targetId){
    //si el array giradas aun no tiene 2 elementos le añadimos el recibido en la funcion
    if(arrayGiradas.length<2){
        arrayGiradas.push(cartaGirada);
        arrayImgGiradas.push(targetId);
    }
    //si el array giradas ya tiene 2 elementos
    if(arrayGiradas.length==2){
        //comprobamos si el id de cada uno de los elementos girados es el mismo0
        if(arrayGiradas[0].getId()==arrayGiradas[1].getId()){

            //Si es el mismo id añadimos borde a las imagenes
            document.getElementById(arrayImgGiradas[0]).style.border="solid";
            document.getElementById(arrayImgGiradas[1]).style.border="solid";
            //quitavos el evento click de los girados correctamente
            document.getElementById(arrayImgGiradas[0]).removeEventListener("click", voltearImagen);
            document.getElementById(arrayImgGiradas[1]).removeEventListener("click", voltearImagen);
            //vaciamos los array para seguir jugando
            arrayGiradas.splice(0, arrayGiradas.length);
            arrayImgGiradas.splice(0, arrayImgGiradas.length);
        }else{ //si los id no son los mismos
            //vaciamos arrayGiradas.
            arrayGiradas.splice(0, arrayGiradas.length);
            //esperamos 3 segundos y configuramos la imagen a reverso de nuevo
            setTimeout(function(){
                document.getElementById(arrayImgGiradas[0]).src="img_cartas/reverso.jpg";
                document.getElementById(arrayImgGiradas[1]).src="img_cartas/reverso.jpg";
                arrayImgGiradas.splice(0, arrayImgGiradas.length);
                }, 1000);
        }
    }
}

